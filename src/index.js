(window => {
    let _timid = {
        settings: {
            api: 'https://api.timid.app',
            service: null
        },
        Init: (settings) => {
            // Load and set settings
            _timid.settings.service = settings.service;
        },

        // User handling
        User: {
            email: null,
            token: null,
            Login: async (email, password, _callback = null) => {
                // Basic verification
                if (email === "" || password === "") {
                    // @TODO: display error
                    return false;
                }

                // @TODO: send login reuqest
                let response = await _timid.Utility.request(
                    'POST', 
                    '/login', 
                    {
                        'email': email,
                        'password': password
                    }
                );

                // Handle the response
                if (response.status === "success") {
                    // Set details
                    _timid.User.email = email;
                    _timid.User.token = response.token;

                    // Run the callback if it exists
                    if (_callback !== null) {
                        _callback();
                    }

                    return true;
                }

                // @TODO: handle the rror

                // Run the callback if it exists
                if (_callback !== null) {
                    _callback();
                }

                return false;
            },
            Logout: async (_callback) => {
                // Send logout request
                let response = await _timid.Utility.request('GET', '/logout', null, {
                    'X-Email': _timid.User.email,
                    'X-Token': _timid.User.token
                });

                // Run the callback if it exists
                if (_callback != null) {
                    _callback();
                }

                // Unset the user data
                _timid.User.email = null;
                _timid.User.token = null;
            },

            // Check to see if the user was validated
            IsValid: () => {
                // make sure the email and token is set
                if (_timid.User.email != null && _timid.User.token != null) {
                    return true;
                }

                return false;
            }
        },

        // API key handling
        Key: {
            key: null,
            _callback: null,
            Get: () => {
                return _timid.Key.key.key;
            },
            Create: (name, permissions, _callback) => {
                // Create a new key
                _timid.Key.key = {
                    "name": name,
                    "service": _timid.settings.service,
                    "permissions": permissions
                }

                // Set the call back so it doesn't get lost
                _timid.Key._callback = _callback;

                // Call the warn function
                _timid.Key.warn();
            },
            warn: () => {
                // @TODO: display warning to user
                _timid.Key.send()
            },
            send: async (_callback) => {
                // Send request to timid to create the key
                let response = await _timid.Utility.request('PUT', '/key', _timid.Key.key, {
                    'X-Email': _timid.User.email,
                    'X-Token': _timid.User.token
                });

                // Key was created
                if (response.status === "success") {
                    // Update settings based on response
                    _timid.User.token = response.token;
                    _timid.Key.key.key = response.key;

                    // Run the callback if it exists
                    if (_callback != null) {
                        _callback();
                    }

                    return response.key;
                }

                // @TODO: handle the error

                // Run the callback if it exists
                if (_callback != null) {
                    _callback();
                }

                return null;
            }
        },

        // Details handling
        Details: {
            details: null,

            // Get details using and API key and the service name
            Fetch: async(key, details, _callback) => {
                // Build the reqeust body
                let req = {}
                if (details.length > 0) {
                    req.request = details;
                }
                
                // Fetch details from the API
                let response = await _timid.Utility.request('GET', '/details', req, {
                    'X-Email': _timid.User.email,
                    'X-Key': key,
                    'X-Service': _timid.settings.service
                });

                // Handle the response
                if (response.status === "success") {
                    // Set the details
                    _timid.Details.details = response.details;

                    // Run the callback if it exists
                    if (_callback != null) {
                        _callback();
                    }

                    return response.details;
                }

                // @TODO: Handle the error

                // Run the callback if it exists
                if (_callback != null) {
                    _callback();
                }

                return null;
            }
        },

        // Misc functions
        Utility: {
            request: async (method = 'GET', route = '', data = {}, headers = {}) => {

                // Build the request settings
                let settings = {
                    method: method,
                    mode: 'cors', // no-cors, *cors, same-origin
                    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                    credentials: 'omit', // include, *same-origin, omit
                    headers: headers,
                    redirect: 'follow',
                    referrerPolicy: 'no-referrer'
                };

                // Set the body if the data isn't null
                if (data != null) {
                    settings.body = JSON.stringify(data)
                }

                // Default options are marked with *
                const response = await fetch(_timid.settings.api + route, settings);
                
                // Parses JSON response into native JavaScript objects
                return response.json(); 
            }
        }
    }

    // Return the pulic parts of the dashboard
    window.TIMID = {
        Init: _timid.Init,
        Login: _timid.User.Login,
        Logout: _timid.User.Logout,
        User: {
            IsValid: _timid.User.IsValid
        },
        Key: {
            Get: _timid.Key.Get,
            Create: _timid.Key.Create
        },
        Details: {
            Fetch: _timid.Details.Fetch
        }
    };
})(window);